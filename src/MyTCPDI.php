<?php

namespace rcamposp\tcpdi_merger;
// Include the main TCPDF library and TCPDI.
use rcamposp\tcpdi_merger\tcpdi;  

#Intended to extend stuff from the base TCPDF class
class MyTCPDI extends TCPDI{
    protected $header_line_color = array(255,255,255);

    protected $showPagination;
    protected $headerData;
    
    public function __construct($showPagination = false, $headerData = array()){        
        $this->headerData = $headerData;
        $this->showPagination = $showPagination;                
        parent::__construct();        
    }

    public function Header(){
        if(!empty($this->headerData)){            
            list($image_1, $image_2, $title_1, $title_2, $title_3) = $this->headerData;
            // Logo        
            $this->Image($image_1, 10, 1, 15, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // Set font
            $this->SetFont('helvetica', '', 8);
            // Title

            $html = '<p>'.$title_1.' - '.$title_2.' - '.$title_3. '</p>';

            // Print text using writeHTMLCell()
            $this->writeHTMLCell(150, 0, '', '', $html, 0, 1, 0, true, 'C', true);            
            
            $this->Image($image_2, 10, 1, 15, '', '', '', 'T', false, 300, 'R', false, false, 0, false, false, false);
            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(221, 221, 221));
            $this->Line(0, 8, 1000, 8, $style);
        }
    }

    // Page footer
	public function Footer() {
        if($this->showPagination){
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'B', 8);
            // Page number
            $this->Cell(0, 10, $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
	}
    
}


?>

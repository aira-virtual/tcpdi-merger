<?php

namespace rcamposp\tcpdi_merger;

#require_once("src/MyTCPDI.php");

class Merger{
    private $tcpdi;

    private $files = array();    

    private $output = "";   

    private $tempDir;    

    public function __construct($showPagination = false, $headerData = array()){
        $this->tcpdi = new MyTCPDI($showPagination, $headerData); //Default page format params             
    }

    public function addRaw($pdf, $name='', $url=''){
        assert('is_string($pdf)');
        // Create temporary file
        $fname = $this->getTempFname();
        if (@file_put_contents($fname, $pdf) === false) {
            throw new Exception("Unable to create temporary file");
        }
        if(!$this->isFileCompatible($fname)){
            throw new Exception("File not readeable");                        
        }
        $this->addFromFile($fname, true, $name);
    }
    
        
    public function addFromFile($fname, $cleanup = false, $name='', $url=''){
        assert('is_string($fname)');
        assert('is_bool($cleanup)');
        if (!is_file($fname) || !is_readable($fname)) {
            throw new Exception("'$fname' is not a valid file");
        }                     
        $this->files[] = array($fname, $cleanup, $name, $url);
    }

    //$tableOfContents (boolean|string): If string, this will be assigned as the title of the TOC  
    public function merge($ignoreIncompatibleFiles=true, $tableOfContents = false, $TOCPageNumber){
        if (empty($this->files)) {
            throw new Exception("Unable to merge, no PDFs added");
        }
        $pageNumber = 1;
        $fname = '';
        try {
            
            $tcpdi = clone $this->tcpdi;

            foreach ($this->files as $fileData) {
                list($fname, $cleanup, $name, $url) = $fileData;
                                

                $iPageCount = $tcpdi->setSourceFile($fname);
                // Add all pages                
                $pages = range(1, $iPageCount);
                           
                // set a bookmark for the current position
                $tcpdi->Bookmark($name, 0, 0, $pageNumber, 'B', array(0,64,128));                
                $pageNumber += count($pages);

                // Add specified pages
                foreach ($pages as $page) {                                        
                    $template = $tcpdi->importPage($page,'/BleedBox');                    
                    $size = $tcpdi->getTemplateSize($template);
                    $orientation = ($size['w'] > $size['h']) ? 'L' : 'P';

                    $tcpdi->AddPage($orientation);                    
                    $tcpdi->useTemplate($template);                    
                    $tcpdi->importAnnotations($page);
                }
            }
            
            if($tableOfContents){
                // add a new page for TOC
                $tcpdi->AddPage('P','','',true);

                // write the TOC title
                $tcpdi->SetFont('dejavusans', 'B', 16);
                $tcpdi->MultiCell(0, 0, $tableOfContents, 0, 'C', 0, 1, '', '', true, 0);
                $tcpdi->Ln();

                $tcpdi->SetFont('dejavusans', '', 12);

                // add a simple Table Of Content at $TOCPageNumber page                
                $tcpdi->addTOC($TOCPageNumber, 'courier', '.', $tableOfContents, 'B', array(128,0,0));

                // end of TOC page
                $tcpdi->endTOCPage();
            }

            $output = $tcpdi->Output('', 'S');
            $tcpdi->cleanUp();
            foreach ($this->files as $fileData) {
                list($fname, $cleanup) = $fileData;
                if ($cleanup) {
                    unlink($fname);
                }
            }
            $this->files = array();            
            $this->output = $output;            
            
        } catch (Exception $e) {
            throw new Exception("FPDI: '{$e->getMessage()}' in '$fname'", 0, $e);
        }
    }

    private function isFileCompatible($filename){
        try{            
            $tcpdi = new MyTCPDI();
            
            $iPageCount = $tcpdi->setSourceFile($filename);
            $pages = range(1, $iPageCount);  

            foreach ($pages as $page) {                 
                $template = $tcpdi->importPage($page,'/BleedBox');                            
                $size = $tcpdi->getTemplateSize($template);
                $orientation = ($size['w'] > $size['h']) ? 'L' : 'P';

                $tcpdi->AddPage($orientation);                    
                $tcpdi->useTemplate($template);                    
                $tcpdi->importAnnotations($page);
            }

            $output = $tcpdi->Output('', 'S');
            return true;
        }catch(\Exception $e){                           
            return false;
        }
    }

    public function getRawOutput(){
        return $this->output;
    }

    public function download($filename = 'document.pdf')
    {    
        header("Content-Type: application/pdf");
        header('Content-Length: '.strlen($this->output));            
        header('Content-disposition: attachment; filename="'.$filename.'"');
        echo $this->output;        
    }

    public function save($path = 'example/document.pdf')
    {
        file_put_contents($path, $this->output);
    }    

    /**
     * Create temporary file and return name
     *
     * @return string
     */
    public function getTempFname()
    {
        return tempnam($this->getTempDir(), "pdfmerge");
    }
    /**
     * Get directory path for temporary files
     *
     * Set path using setTempDir(), defaults to sys_get_temp_dir().
     *
     * @return string
     */
    public function getTempDir()
    {
        return $this->tempDir ?: sys_get_temp_dir();
    }
	

    /**
     * Set directory path for temporary files
     *
     * @param  string $dirname
     * @return void
     */
    public function setTempDir($dirname)
    {
        $this->tempDir = $dirname;
    }
}

class Exception extends \Exception{}

?>